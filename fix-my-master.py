import os
import subprocess
import sys


def main():
    cwd_path = os.path.dirname(os.path.realpath(__file__))
    if len(sys.argv) > 1:
        cwd_path = sys.argv[1]

    result = subprocess.run(['git', 'ls-files', '-c', '-i', '-X', '.gitignore'], stdout=subprocess.PIPE, cwd=cwd_path)
    result_lines = result.stdout.decode('utf-8').strip('\n').split('\n')
    for filename in result_lines:
        result = subprocess.run(['git', 'rm', '--cached', '--', filename], stdout=subprocess.PIPE, cwd=cwd_path)
        if result.returncode != 0:
            print("{} was not removed from git!".format(filename))
            print(result.stdout.decode('utf-8'))
            print(result.stderr.decode('utf-8'))
        else:
            print("{} was removed from tracked git objects".format(filename))
    print("Process finished, now you can commit and push the changes.")
    print('Use: git commit -m "Fixed tracked files that should be ignored per the .gitignore" && git push')


if __name__ == '__main__':
    main()
